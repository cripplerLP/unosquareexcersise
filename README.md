# UnoSquare Excersise

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.2.0.
And also with Express

## Build

To run the aplication you must execute the command `npm install` in both directories backend and frontend

Next to start the webserver go to backend directory and execute `node index.js`.

And to start the frontend app go to the frontend directory and execute `npm run start`;

now go to [http://localhost:4200/](http://localhost:4200/) to see the app running

## Running unit tests

Run `ng test` in the frontend directory to execute the unit tests via [Karma](https://karma-runner.github.io).
