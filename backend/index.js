const express = require('express');
const https = require('https');
const stream = require('stream');
var cors = require('cors');
const fs = require('fs');

const app = express();

app.use(express.json());
app.use(cors());

const dirExists = fs.existsSync('images');
if (!dirExists) {
    fs.mkdirSync('images');
}

app.post('/api/save', (req, response, next) => {
    let {url, name} = req.body;
    console.log(url, name);
    let myStream = new stream.Transform();
    https.request(url, (res) => {
        res.on('data', (chunk) => { myStream.push(chunk) })
        res.on('end', () => {
            fs.writeFileSync(`images/${name}.png`, myStream.read());
            response.send({message:'pokemon saved'});
        })
    }).end();
});

app.listen(8000, () => {
    console.log('Server running');
});