import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  apiURL = 'https://pokeapi.co/api/v2/';
  localAPI = 'http://localhost:5000/api/';

  constructor(private _http: HttpClient) { }

  searchPokemon(pokemonName) {
    return this._http.get(`${this.apiURL}pokemon/${pokemonName}`).toPromise();
  }

  savePokemon(pokemon) {
    return this._http.post(`/api/save`, {name: pokemon.name, url: pokemon.sprites['front_default']});
  }
}
