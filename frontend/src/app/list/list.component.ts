import { Component, OnInit, Input } from '@angular/core';
import { PokemonService } from '../pokemon.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  @Input() pokemons:any[];

  constructor(private _service: PokemonService) { }

  ngOnInit() {
  }

  savePokemon(pokemon) {
    this._service.savePokemon(pokemon).toPromise().then((res => {
      console.log(res);
    }));
  }

}
