import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListComponent } from './list.component';
import { By } from '@angular/platform-browser';

describe('ListComponent', () => {
  let component: ListComponent;
  let fixture: ComponentFixture<ListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListComponent);
    component = fixture.componentInstance;
    component.pokemons = [];
    fixture.detectChanges();
  });

  it('should create', () => {
    component.pokemons = [];
    expect(component).toBeTruthy();
  });

  it('should display default message when list is empty', () => {

    component.pokemons = [];
    fixture.detectChanges();
    const textEl = fixture.debugElement.query(By.css('#default-message'));
    expect(textEl.nativeElement.textContent).toContain('No pokemon captured yet');
  });

  it('should display a pokemon when the list is not empty', () => {
    component.pokemons = [
      {
        id: 25,
        name: "pikachu",
        sprites: {
          front_default: "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/25.png"
        }
      }
    ];
    fixture.detectChanges();
    const pokemonEl = fixture.debugElement.query(By.css('.pokemon-detail'));
    expect(pokemonEl).toBeTruthy();
  })
});
