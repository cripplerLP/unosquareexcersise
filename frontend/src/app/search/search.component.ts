import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { PokemonService } from '../pokemon.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  search: string;
  @Output() onSearch: EventEmitter<any> = new EventEmitter();

  constructor(private _service: PokemonService) { }

  ngOnInit() {
  }

  searchPokemon() {
    this._service.searchPokemon(this.search).then((val) => {
      console.log(val);
      this.onSearch.emit(val);
    }, (err) => {
      console.error(err.message);
    });
  }
}
